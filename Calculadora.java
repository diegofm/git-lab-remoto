import java.util.Scanner;
public class Calculadora {

    public static void main (String[] args) {
        System.out.println("Calculadora Simples");

        int opcao;
        do {
            System.out.println("1-Somar");
            System.out.println("2-Subtrair");
            System.out.println("3-Multiplicar");
            System.out.println("4-Dividir");
            System.out.print("Qual operação deseja realizar? (0 para sair): ");

            Scanner calculadora = new Scanner (System.in);
            opcao = calculadora.nextInt();

            processar(opcao);

        } while (opcao != 0);       

    }

    public static void processar (int opcao) {

        Scanner calculadora = new Scanner (System.in);

        switch (opcao) {
            case 1: {                
                System.out.println("Somando dois Números");

                System.out.print("Digite o primeiro número: ");
                int numero1 = calculadora.nextInt();
                System.out.print("Digite o segundo número: ");
                int numero2 = calculadora.nextInt();

                int soma = numero1 + numero2;
                System.out.println("Somátorio: " + soma);
                break;
            }
            case 2: {                
                System.out.println("Subtraindo dois Números");

                System.out.print("Digite o primeiro número: ");
                int numero1 = calculadora.nextInt();
                System.out.print("Digite o segundo número: ");
                int numero2 = calculadora.nextInt();

                int subtracao = numero1 - numero2;
                System.out.println("Somátorio: " + subtracao);
                break;
            }
                
            case 3: {                
                System.out.println("Multiplicando dois Números");

                System.out.print("Digite o primeiro número: ");
                int numero1 = calculadora.nextInt();
                System.out.print("Digite o segundo número: ");
                int numero2 = calculadora.nextInt();

                int multiplicar = numero1 * numero2;
                System.out.println("Somátorio: " + multiplicar);
                break;
            }
            case 4: {                
                System.out.println("Dividindo dois Números");

                System.out.print("Digite o primeiro número: ");
                double numero1 = calculadora.nextInt();
                System.out.print("Digite o segundo número: ");
                double numero2 = calculadora.nextInt();
                if (numero2 == 0) {
                    System.out.println("O número 0 não é divisível!");
                }
                else {
                    double dividir = numero1 / numero2;
                System.out.println("Somátorio: " + dividir);
                }                
                break;
            }
        }
    }
    }